from parsers import parse_file


def test_parser(input_file, input_format):
    parse_file(input_file, input_format)


def main():
    test_parser("./samples/bank1.csv", "bank1")
    test_parser("./samples/bank2.csv", "bank2")
    test_parser("./samples/bank3.csv", "bank3")


if __name__ == '__main__':
    main()
